package org.moresbycoffee.cassandrabug;

import com.datastax.driver.core.Cluster;
import lombok.val;

import java.util.concurrent.ExecutionException;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

/**
 * Created by barnabassudy on 19/10/2014.
 */
public class Application {

    public static void main(final String[] args) throws InterruptedException, ExecutionException {
        createKeyspace();
        try(val cluster = Cluster.builder().addContactPoint("localhost").build()) {
            try(val session = cluster.connect()) {
                val rs = session.executeAsync("SELECT * FROM system.schema_usertypes");
                for (val row : rs.get()) {
                    System.out.println("Direct select result row: " + row);
                }
            }
            for (val ut : cluster.getMetadata().getKeyspace("udtbug").getUserTypes()) {
                System.out.println("Metadate User Type: " + ut);
                System.out.println("Metadate User Type fields: " + ut.getFieldNames());
                assertThat("Usertype should have field", ut.getFieldNames(), not(empty()));
            }
        } finally {
            dropKeyspace();
        }
    }

    private static void createKeyspace() {
        try(val cluster = Cluster.builder().addContactPoint("localhost").build()) {
            try(val session = cluster.connect()) {
                session.execute("CREATE KEYSPACE udtbug WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 1 }");
                session.execute("create type udtbug.mytype (testfield varchar);");
            }
        }
    }

    private static void dropKeyspace() {
        try(val cluster = Cluster.builder().addContactPoint("localhost").build()) {
            try(val session = cluster.connect()) {
                session.execute("DROP KEYSPACE udtbug");
            }
        }
    }
}
